﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetMouseCursorPointerTrigger : TriggerInterface
{

    public Texture2D cursorTexture;
    public Vector2 offset = Vector2.zero;

    public override void Cancel()
    {
        
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        Cursor.SetCursor(cursorTexture, offset, CursorMode.Auto);
    }

}
