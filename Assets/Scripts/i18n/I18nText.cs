﻿using UnityEngine;
using UnityEngine.UI;

public class I18nText : MonoBehaviour
{

#if UNITY_EDITOR
    public I18nManager i18nManager;
#endif

    public string I18nFolder;
    public string I18nName;
    protected Text text;

    void Start()
    {
        UpdateText();
    }

    public void UpdateText()
    {
        if (text == null)
        {
            text = GetComponent<Text>();
        }
        text.text = I18nManager.Instance.GetFolderItem(I18nFolder, I18nName);
    }

}
