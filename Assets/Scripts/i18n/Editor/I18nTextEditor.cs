﻿using UnityEditor;

[CustomEditor(typeof(I18nText))]
public class I18nTextEditor : EditorBase
{
    protected void SetUpPrefabConflict(I18nText text)
    {
        if (EditorApplication.isPlaying)
            return;
        Undo.RecordObject(text, "I18nText");
    }

    protected void StorePrefabConflict(I18nText text)
    {
        if (EditorApplication.isPlaying)
            return;
        if (!_hadChanges)
        {
            return;
        }
        EditorUtility.SetDirty(text);
        PrefabUtility.RecordPrefabInstancePropertyModifications(this);
        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(text.gameObject.scene);
    }

    public override void OnInspectorGUI()
    {
        I18nText text = (I18nText)target;
        SetUpPrefabConflict(text);

        text.i18nManager = EditorUtils.I18nManagerField(
            this,
            "I18nManager",
            text.i18nManager
        );

        if (text.i18nManager == null)
        {
            EditorGUILayout.HelpBox("Please assign an I18nManager", MessageType.Warning);
        }
        else
        {
            string[] folders = new string[text.i18nManager.Folders.Length];
            int folderIndex = 0;
            for(int iFolder=0; iFolder < text.i18nManager.Folders.Length; iFolder++)
            {
                folders[iFolder] = text.i18nManager.Folders[iFolder].Name;
                if (folders[iFolder].Equals(text.I18nFolder))
                {
                    folderIndex=iFolder;
                }
            }
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Carpeta");
            folderIndex = EditorGUILayout.Popup(folderIndex, folders);
            EditorGUILayout.EndHorizontal();
            if (
                (string.IsNullOrEmpty(text.I18nFolder) && !string.IsNullOrEmpty(folders[folderIndex])) ||
                (!string.IsNullOrEmpty(text.I18nFolder) && string.IsNullOrEmpty(folders[folderIndex])) ||
                (text.I18nFolder != null && !text.I18nFolder.Equals(folders[folderIndex]))
            )
            {
                text.I18nFolder = folders[folderIndex];
                _hadChanges = true;
            }

            string[] items = new string[text.i18nManager.Folders[folderIndex].Items.Length];
            int itemIndex = 0;
            for (int iItem = 0; iItem < text.i18nManager.Folders[folderIndex].Items.Length; iItem++)
            {
                items[iItem] = text.i18nManager.Folders[folderIndex].Items[iItem].Name;
                if (items[iItem].Equals(text.I18nName))
                {
                    itemIndex = iItem;
                }
            }
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Item");
            itemIndex = EditorGUILayout.Popup(itemIndex, items);
            EditorGUILayout.EndHorizontal();
            if (
                (string.IsNullOrEmpty(text.I18nName) && !string.IsNullOrEmpty(items[itemIndex])) ||
                (!string.IsNullOrEmpty(text.I18nName) && string.IsNullOrEmpty(items[itemIndex])) ||
                (text.I18nName != null && !text.I18nName.Equals(items[itemIndex]))
            )
            {
                text.I18nName = items[itemIndex];
                _hadChanges = true;
            }
        }



        text.UpdateText();
        StorePrefabConflict(text);
    }

}
