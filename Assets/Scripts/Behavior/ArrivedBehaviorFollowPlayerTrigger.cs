﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrivedBehaviorFollowPlayerTrigger : TriggerInterface
{
    public BehaviorFollowPlayer behavior;

    public override void Cancel()
    {

    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        behavior.SignalArrived();
    }

}
