﻿using UnityEditor;

[CustomEditor(typeof(BehaviorFollowPlayer), true)]
public class BehaviorFollowPlayerEditor : EditorBase
{

    protected void SetUpPrefabConflict(BehaviorFollowPlayer behavior)
    {
        if (EditorApplication.isPlaying)
            return;
        Undo.RecordObject(behavior, "BehaviorFollowPlayer");
    }

    protected void StorePrefabConflict(BehaviorFollowPlayer behavior)
    {
        if (EditorApplication.isPlaying)
            return;
        if (!_hadChanges)
        {
            return;
        }
        EditorUtility.SetDirty(behavior);
        PrefabUtility.RecordPrefabInstancePropertyModifications(this);
        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(behavior.gameObject.scene);
    }

    public override void OnInspectorGUI()
    {
        _hadChanges = false;
        BehaviorFollowPlayer behaviorFollowPlayer = (BehaviorFollowPlayer)target;
        SetUpPrefabConflict(behaviorFollowPlayer);
        behaviorFollowPlayer.OnStartFollow = EditorUtils.SelectTrigger(this,"On Start Follow", behaviorFollowPlayer.OnStartFollow);
        behaviorFollowPlayer.OnStopFollow = EditorUtils.SelectTrigger(this, "On Stop Follow", behaviorFollowPlayer.OnStopFollow);
        behaviorFollowPlayer.MovementSpeed = EditorUtils.FloatField(this,"Movement Speed", behaviorFollowPlayer.MovementSpeed);
        behaviorFollowPlayer.StopOnArrive = EditorUtils.Checkbox(this, "Stop On Arrive", behaviorFollowPlayer.StopOnArrive);
        if (behaviorFollowPlayer.StopOnArrive)
        {
            behaviorFollowPlayer.MinDistanceToGoal = EditorUtils.FloatField(this,"Min Distance To Goal", behaviorFollowPlayer.MinDistanceToGoal);
            behaviorFollowPlayer.OnArrive = EditorUtils.SelectTrigger(this, "On Arrive", behaviorFollowPlayer.OnArrive);
        }
        StorePrefabConflict(behaviorFollowPlayer);
    }
}
