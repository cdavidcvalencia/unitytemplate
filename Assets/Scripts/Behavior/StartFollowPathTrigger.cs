using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartFollowPathTrigger : TriggerInterface
{

    public FollowPath followPath;

    public override void Cancel()
    {
        
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        if (followPath != null)
        {
            followPath.StartMovement();
        }
    }

}
