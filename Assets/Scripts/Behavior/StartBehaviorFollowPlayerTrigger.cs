﻿public class StartBehaviorFollowPlayerTrigger : TriggerInterface
{

    public BehaviorFollowPlayer behavior;

    public override void Cancel()
    {

    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        behavior.StartFollowing();
    }

}
