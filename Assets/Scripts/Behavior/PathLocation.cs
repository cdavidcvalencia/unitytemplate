﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PathLocation
{

#if UNITY_EDITOR
    [HideInInspector]
    public bool showLocation = false;
#endif

    public Vector3 Location=Vector3.zero;
    public float WaitTimeAfterArrival;
    public TriggerInterface OnArriveTrigger;
    public TriggerInterface OnLeaveTrigger;

}
