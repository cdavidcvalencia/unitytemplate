using UnityEditor;

[CustomEditor(typeof(TriggerDebugCheck))]
public class TriggerDebugCheckEditor : TriggerInterfaceEditor
{
    protected void SetUpPrefabConflict(TriggerDebugCheck trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        Undo.RecordObject(trigger, "TriggerDebugCheck");
    }

    protected void StorePrefabConflict(TriggerDebugCheck trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        if (!_hadChanges)
        {
            return;
        }
        EditorUtility.SetDirty(trigger);
        PrefabUtility.RecordPrefabInstancePropertyModifications(this);
        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(trigger.gameObject.scene);
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        TriggerDebugCheck triggerDebugCheck = (TriggerDebugCheck)target;
        SetUpPrefabConflict(triggerDebugCheck);
        triggerDebugCheck.Text = EditorUtils.TextField(
            this,
            "Text",
            triggerDebugCheck.Text
        );
        StorePrefabConflict(triggerDebugCheck);
    }

}
