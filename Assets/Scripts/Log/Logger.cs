﻿using System.IO;
using System.Text;

public class Logger
{
    
    public static void Log(string text)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append(text);
        sb.Append("\n");
        File.AppendAllText("log.txt", sb.ToString());
        sb.Clear();
    }

}
