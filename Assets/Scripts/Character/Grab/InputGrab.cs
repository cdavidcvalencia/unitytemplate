﻿using System.Collections.Generic;
using UnityEngine;

public class InputGrab : MonoBehaviour
{

    public string InputName;
    public float Cooldown = 0.1f;

    protected GrabHandle handle;
    protected List<Grabbable> grabbables=new List<Grabbable>();
    protected bool Ready = true;
    protected float CooldownTimer = 0f;

    private void Awake()
    {
        handle = GetComponent<GrabHandle>();
    }

    public void OnTriggerEnter(Collider c)
    {
        Grabbable grab = c.GetComponent<Grabbable>();
        if (grab != null && !grabbables.Contains(grab))
        {
            grabbables.Add(grab);
        }
    }

    public void OnTriggerExit(Collider c)
    {
        Grabbable grab = c.GetComponent<Grabbable>();
        if (grab != null&&grabbables.Contains(grab))
        {
            grabbables.Remove(grab);
        }
    }

    void Update()
    {
        if (!Ready)
        {
            CooldownTimer += Time.deltaTime;
            if (CooldownTimer >= Cooldown)
            {
                Ready = true;
            }
        }
        if (Input.GetAxis(InputName) != 0 && Ready)
        {
            if (handle.getTarget() == null)
            {
                CleanGrabbables();
                if (grabbables.Count > 0)
                {
                    handle.Grab(grabbables[0]);
                    CooldownTimer = 0f;
                    Ready = false;
                }
            }
            else
            {
                handle.Release();
                CooldownTimer = 0f;
                Ready = false;
            }
        }
    }

    protected void CleanGrabbables()
    {
        while (grabbables.Count > 0 && grabbables[0] == null)
        {
            grabbables.RemoveAt(0);
        }
    }

    public void SetReady(bool ready)
    {
        enabled = ready;
        Ready = ready;
    }

}
