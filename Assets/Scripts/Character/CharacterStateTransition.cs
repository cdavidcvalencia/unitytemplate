﻿using UnityEngine;

namespace mw.player
{
    [System.Serializable]
    public class CharacterStateTransition
    {

        public string Name;
        public TriggerInterface BeforeTransition;
        public CharacterState NextState;

    }
}