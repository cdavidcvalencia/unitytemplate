﻿using mw.player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenericState : CharacterState
{

    public override void OnEnterState()
    {
        
    }

    public override void OnExitState()
    {
        
    }

    public override void OnFixedUpdate()
    {
        
    }

    public override void OnLateUpdate()
    {

    }

    public override void OnPauseState()
    {
        
    }

    public override void OnResumeState()
    {
        
    }

    public override void OnStimuli(string stimuli, GameObject source)
    {
    }

    public override void OnUpdate()
    {
        
    }
}
