﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeManagerEvent : MonoBehaviour
{

    public string Name;
    public int days;
    public int hours;
    public int minutes;
    public int seconds;
    public TriggerInterface Trigger;

}
