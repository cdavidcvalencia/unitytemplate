﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct SavedInt
{
    public string Name;
    public int Value;
}
