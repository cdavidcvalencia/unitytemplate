﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetSavedDataTrigger : TriggerInterface
{

    public bool AutoSave = true;
    public string DataName;

    public override void Cancel()
    {
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {

        }
        SetSavedData();
        if (AutoSave)
        {
            PersistanceManager.Instance.SaveData();
        }
    }

    public virtual void SetSavedData()
    {

    }

    public override string GetDescription()
    {
        string desc = base.GetDescription() + " (" + DataName + ")";
        return desc;
    }

}
