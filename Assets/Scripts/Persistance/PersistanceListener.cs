﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersistanceListener : MonoBehaviour
{

    public string DataName;

    public TriggerInterface OnSetData;
    public TriggerInterface OnLoadData;
    public TriggerInterface OnSaveData;

    private bool _registered = false;

    void Awake()
    {
        Register();
    }

    public void Register()
    {
        if (_registered)
        {
            return;
        }
        _registered = PersistanceManager.Instance.RegisterListener(this);
    }

    public void TriggerEvent(PersistanceManager.EventType eventType, string DataName = "", PersistanceManager.DataType dataType = PersistanceManager.DataType.Bool)
    {
        switch (eventType)
        {
            case PersistanceManager.EventType.Load:
                if (OnLoadData != null)
                {
                    OnLoadData.Fire();
                }
                break;
            case PersistanceManager.EventType.Save:
                if (OnSaveData != null)
                {
                    OnSaveData.Fire();
                }
                break;
            case PersistanceManager.EventType.Set:
                if (OnSetData != null)
                {
                    OnSetData.Fire();
                }
                break;
        }
    }

}
