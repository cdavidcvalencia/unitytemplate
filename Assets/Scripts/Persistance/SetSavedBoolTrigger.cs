﻿public class SetSavedBoolTrigger : SetSavedDataTrigger
{

    public bool Value;

    public override void SetSavedData()
    {
        PersistanceManager.Instance.SetSavedData(DataName, Value);
    }
}
