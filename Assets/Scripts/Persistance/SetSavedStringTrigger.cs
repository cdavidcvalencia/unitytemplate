﻿public class SetSavedStringTrigger : SetSavedDataTrigger
{
    public string Value;

    public override void SetSavedData()
    {
        PersistanceManager.Instance.SetSavedData(DataName, Value);
    }
}
