﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct SavedString
{
    public string Name;
    public string Value;
}
