﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace mw.Pathfinding
{
    public class Pathfinder
    {
        AINavMeshGenerator generator;

        List<Node> openSet = new List<Node>();
        HashSet<Node> closedSet = new HashSet<Node>();
        List<Node> path = new List<Node>();
        public Pathfinder(AINavMeshGenerator gen)
        {
            generator = gen;
        }

        private float Heuristic(Node one, Node two)
        {
            return Vector2.Distance(one.position, two.position);
        }

        private Node[] GetAStar(Vector2 currentPosition, Vector2 destination, GameObject obj = null)
        {
            Node start = generator.FindClosestNode(currentPosition, false, obj);
            Node end = generator.FindClosestNode(destination, true, obj);
            if (start == null || end == null)
            {
                return null;
            }

            openSet.Clear();
            closedSet.Clear();
            openSet.Add(start);
            while (openSet.Count > 0)
            {
                Node current = openSet[0];

                //Evaluate costs
                for (int i = 1; i < openSet.Count; i++)
                {
                    if (openSet[i].fCost < current.fCost || openSet[i].fCost == current.fCost)
                    {
                        if (openSet[i].hCost < current.hCost)
                        {
                            current = openSet[i];
                        }
                    }
                }

                openSet.Remove(current);
                closedSet.Add(current);

                if (current.Equals(end))
                {
                    break;
                }

                //Go through neighbors
                foreach (Node neighbor in current.connections.Where(x => x != null))
                {
                    //The associated object check is so the enemy ignores pathing through it's own bad sector
                    if ((!neighbor.valid && neighbor.associatedObject != obj) || closedSet.Contains(neighbor))
                    {
                        continue;
                    }

                    float newCost = current.gCost + Heuristic(current, neighbor);
                    if (newCost < neighbor.gCost || !openSet.Contains(neighbor))
                    {
                        neighbor.gCost = newCost;
                        neighbor.hCost = Heuristic(neighbor, end);
                        neighbor.parent = current;

                        if (!openSet.Contains(neighbor))
                        {
                            openSet.Add(neighbor);
                        }
                    }
                }
            }

            if (end.parent == null)
            {
                return null;
            }

            //Calculate path
            path.Clear();
            Node currentCheck = end;
            while (!path.Contains(currentCheck) && currentCheck != null)
            {
                path.Add(currentCheck);
                currentCheck = currentCheck.parent;
            }
            path.Reverse();
            if (path[0] != start)
            {
                return null;
            }
            return path.ToArray();
        }

        private List<Vector2> ShortenPointsByVisibility(Node[] points)
        {
            //If we have small amount of points, dont bother with all this.
            //if(points.Length < 2)
            {
                List<Vector2> p = new List<Vector2>();
                for (int i = 0; i < points.Length; i++)
                {
                    p.Add(points[i].position);
                }
                return p;
            }

            List<Vector2> corners = new List<Vector2>();
            corners.Add(points[0].position);
            Node start = points[0];
            Node end = points[1];
            //Go through all the points, starting at 1 (since we already set our initial start to the first point)
            for (int i = 1; i < points.Length; i++)
            {
                //Set the end to our current point and check if its in a bad spot to hang out in town.
                end = null;
                end = points[i];
                bool inBadArea = end.AnyConnectionsBad();

                if (inBadArea)
                {
                    //If it's a bad boy, we add it to our corners, so we walk this way.
                    corners.Add(end.position);
                    //Then start anew. 
                    start = null;
                    start = end;
                }
            }
            //Add that last rebel into the mix for sure.
            corners.Add(points[points.Length - 1].position);

            return corners;
        }

        public Vector2[] FindPath(Vector2 currentPosition, Vector2 destination, GameObject associatedObject = null)
        {
            Node[] points = GetAStar(currentPosition, destination, associatedObject);
            if (points == null)
            {
                return null;
            }

            List<Vector2> shortPath = ShortenPointsByVisibility(points);
            //shortPath.Insert(0, currentPosition);
            return shortPath.ToArray();
        }
    }
}