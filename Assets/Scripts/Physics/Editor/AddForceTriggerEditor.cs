using UnityEditor;

[CustomEditor(typeof(AddForceTrigger))]
public class AddForceTriggerEditor : TriggerInterfaceEditor
{
    protected void SetUpPrefabConflict(AddForceTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        Undo.RecordObject(trigger, "AddForceTrigger");
    }

    protected void StorePrefabConflict(AddForceTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        if (!_hadChanges)
        {
            return;
        }
        EditorUtility.SetDirty(trigger);
        PrefabUtility.RecordPrefabInstancePropertyModifications(this);
        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(trigger.gameObject.scene);
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        AddForceTrigger addForceTrigger = (AddForceTrigger)target;
        SetUpPrefabConflict(addForceTrigger);
        addForceTrigger.showAddForceTrigger = EditorGUILayout.Foldout(
            addForceTrigger.showAddForceTrigger,
            "Force"
        );
        if (addForceTrigger.showAddForceTrigger)
        {
            addForceTrigger.body = EditorUtils.RigidbodyField(
                this,
                "Rigidbody",
                addForceTrigger.body
            );
            addForceTrigger.direction = EditorUtils.Vector3Field(
                this,
                "Direction",
                addForceTrigger.direction
            );
            addForceTrigger.force = EditorUtils.FloatField(
                this,
                "Force",
                addForceTrigger.force
            );
        }
        StorePrefabConflict(addForceTrigger);
    }
}
