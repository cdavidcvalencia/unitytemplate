﻿using UnityEngine;

public class Add2DForceTrigger : TriggerInterface
{

#if UNITY_EDITOR
    public bool showAdd2DForceTrigger = true;
#endif

    public static string[] TriggerCategories = { "Trigger" , "2D Physics" };

    public Rigidbody2D body2D;
    public Vector2 direction;
    public float force;

    public override void Cancel()
    {
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        body2D.AddForce(direction * force);
    }

}
