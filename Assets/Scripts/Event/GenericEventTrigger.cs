﻿public class GenericEventTrigger : TriggerInterface
{

#if UNITY_EDITOR
    public bool showGenericEventTrigger = true;
#endif

    public string EventName;
    public string EventParameter;
    public float EventNumber;
    public int EventInteger;

    public override void Cancel()
    {
        
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        GenericEventManager.Instance.TriggerStringEvent(gameObject,EventName,EventParameter, EventNumber, EventInteger);
    }

    public override string GetDescription()
    {
        return base.GetDescription() + " (" + EventName + ", " + EventParameter+ ", " + EventNumber + ")";
    }

}
