﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchIntParameterTrigger : TriggerInterface
{
    public SwitchIntParameterCaseTrigger[] Cases;

    public override void Cancel()
    {

    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        foreach (SwitchIntParameterCaseTrigger theCase in Cases)
        {
            if (theCase != null && theCase.ParameterValue==IntParameter && theCase.Trigger != null)
            {
                theCase.Trigger.Fire(this);
            }
        }
    }

}
