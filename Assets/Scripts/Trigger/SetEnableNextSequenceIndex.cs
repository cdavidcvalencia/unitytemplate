﻿using System;

public class SetEnableNextSequenceIndex : TriggerInterface
{
    public EnableNextSequenceTrigger enableNextSequence;
    public int Index;

    public override void Cancel()
    {

    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        enableNextSequence.SetIndex(Index);
    }

}
