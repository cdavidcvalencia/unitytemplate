﻿using System;
using UnityEngine;

public enum ConnectionPointType { In, Out }

public class TriggerEditorConnectionPoint
{
    public Rect rect;

    public ConnectionPointType type;

    public TriggerEditorNode node;

    public GUIStyle style;

    public Action<TriggerEditorConnectionPoint> OnClickConnectionPoint;

    public float yOffset;

    public TriggerCallback callback;

    public TriggerEditorConnectionPoint(TriggerEditorNode node, TriggerCallback callback, float yOffset, ConnectionPointType type, GUIStyle style, Action<TriggerEditorConnectionPoint> OnClickConnectionPoint)
    {
        this.callback = callback;
        this.yOffset = yOffset;
        this.node = node;
        this.type = type;
        this.style = style;
        this.OnClickConnectionPoint = OnClickConnectionPoint;
        switch (type)
        {
            case ConnectionPointType.In:
                rect = new Rect(0, 0, 30f, 50f);
                break;

            case ConnectionPointType.Out:
                rect = new Rect(0, 0, 30f, 20f);
                break;
        }
    }

    public void Draw()
    {
        
        switch (type)
        {
            case ConnectionPointType.In:
                rect.y = node.rect.y + yOffset;
                rect.x = node.rect.x - rect.width + 8f;
                break;

            case ConnectionPointType.Out:
                rect.y = node.rect.y + yOffset;
                rect.x = node.rect.x + node.rect.width - 8f;
                break;
        }

        if (GUI.Button(rect, "", style))
        {
            if (OnClickConnectionPoint != null)
            {
                OnClickConnectionPoint(this);
            }
        }
    }
}