﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(OnStartTrigger))]
public class OnStartTriggerEditor : EditorBase
{

    protected void SetUpPrefabConflict(OnStartTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        Undo.RecordObject(trigger, "OnStartTrigger");
    }

    protected void StorePrefabConflict(OnStartTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        if (!_hadChanges)
        {
            return;
        }
        EditorUtility.SetDirty(trigger);
        PrefabUtility.RecordPrefabInstancePropertyModifications(this);
        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(trigger.gameObject.scene);
    }

    public override void OnInspectorGUI()
    {
        OnStartTrigger onStartTrigger = (OnStartTrigger)target;
        SetUpPrefabConflict(onStartTrigger);
        onStartTrigger.Trigger = EditorUtils.SelectTrigger(this,"Trigger", onStartTrigger.Trigger);
        StorePrefabConflict(onStartTrigger);
    }
}
