﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class TriggerEditorNode
{
    public Rect rect;
    public bool isDragged;
    public bool isSelected;

    public TriggerEditorConnectionPoint inPoint;
    public List<TriggerEditorConnectionPoint> outPoints=new List<TriggerEditorConnectionPoint>();

    public GUIStyle style;
    public GUIStyle defaultNodeStyle;
    public GUIStyle selectedNodeStyle;

    public Action<TriggerEditorNode> OnRemoveNode;

    public TriggerInvoker invoker;
    public TriggerInterface trigger;

    private float ItemHeight = 25;
    private int numItems = 2;
    private TriggerCallback[] callbacks;

    public TriggerEditorNode(TriggerInterface trigger,TriggerInvoker invoker, GUIStyle nodeStyle, GUIStyle selectedStyle, GUIStyle inPointStyle, GUIStyle outPointStyle, Action<TriggerEditorConnectionPoint> OnClickInPoint, Action<TriggerEditorConnectionPoint> OnClickOutPoint, Action<TriggerEditorNode> OnClickRemoveNode)
    {
        this.trigger = trigger;
        this.invoker = invoker;
        if (trigger == null)
        {
            numItems--;
        }
        callbacks = invoker.GetCallbacks();
        int itemIndex = numItems;
        if (callbacks != null)
        {
            foreach(TriggerCallback callback in callbacks)
            {
                TriggerEditorConnectionPoint outPoint= new TriggerEditorConnectionPoint(this, callback, (ItemHeight / 2) + (ItemHeight * itemIndex), ConnectionPointType.Out, outPointStyle, OnClickOutPoint);
                itemIndex++;
                outPoints.Add(outPoint);
            }
            numItems += callbacks.Length;
        }
        Vector2 pos = invoker.GetNodeEditorPos();
        rect = new Rect(pos.x, pos.y, 200, ItemHeight*(numItems+1));
        style = nodeStyle;
        if (trigger != null)
        {
            inPoint = new TriggerEditorConnectionPoint(this, null, ItemHeight / 2, ConnectionPointType.In, inPointStyle, OnClickInPoint);
        }
        defaultNodeStyle = nodeStyle;
        selectedNodeStyle = selectedStyle;
        OnRemoveNode = OnClickRemoveNode;
    }

    public void Drag(Vector2 delta)
    {
        rect.position += delta;
        invoker.SetNodeEditorPos(rect.position);
    }

    public void Draw()
    {
        if (inPoint != null)
        {
            inPoint.Draw();
        }
        foreach(TriggerEditorConnectionPoint point in outPoints)
        {
            point.Draw();
        }
        if (trigger != null)
        {
            string description = "";
            description = trigger.GetDescription();
            if (string.IsNullOrEmpty(description))
            {
                description = "(trigger has no description)";
            }
            GUI.Box(rect, new GUIContent("", description), style);
        }
        else
        {
            GUI.Box(rect, "", style);
        }
        int itemIndex = 0;
        GUI.Label(new Rect(rect.x + 10, rect.y +(ItemHeight/2)+(ItemHeight*itemIndex), rect.width - 10, ItemHeight), invoker.GetType().Name);
        itemIndex++;
        if (trigger != null)
        {
            string label = trigger.GetLabel();
            if (string.IsNullOrEmpty(label))
            {
                label = "(trigger has no label)";
            }
            GUI.Label(new Rect(rect.x + 10, rect.y + (ItemHeight / 2) + (ItemHeight * itemIndex), rect.width - 10, ItemHeight), label);
            itemIndex++;
        }
        if (callbacks != null)
        {
            foreach (TriggerCallback callback in callbacks)
            {
                GUI.Label(new Rect(rect.x + 10, rect.y + (ItemHeight / 2) + (ItemHeight * itemIndex), rect.width - 10, ItemHeight), callback.Name);
                itemIndex++;
            }
        }
    }

    public bool HasOutPoint(TriggerEditorConnectionPoint target)
    {
        foreach (TriggerEditorConnectionPoint point in outPoints)
        {
            if (target == point)
            {
                return true;
            }
        }
        return false;
    }

    public bool ProcessEvents(Event e)
    {
        switch (e.type)
        {
            case EventType.MouseDown:
                if (e.button == 0)
                {
                    if (rect.Contains(e.mousePosition))
                    {
                        isDragged = true;
                        GUI.changed = true;
                        isSelected = true;
                        style = selectedNodeStyle;
                    }
                    else
                    {
                        GUI.changed = true;
                        isSelected = false;
                        style = defaultNodeStyle;
                    }
                }

                if (e.button == 1 && isSelected && rect.Contains(e.mousePosition))
                {
                    ProcessContextMenu();
                    e.Use();
                }
                break;

            case EventType.MouseUp:
                isDragged = false;
                break;

            case EventType.MouseDrag:
                if (e.button == 0 && isDragged)
                {
                    Drag(e.delta);
                    e.Use();
                    return true;
                }
                break;
        }

        return false;
    }

    private void ProcessContextMenu()
    {
        GenericMenu genericMenu = new GenericMenu();
        genericMenu.AddItem(new GUIContent("Remove node"), false, OnClickRemoveNode);
        genericMenu.ShowAsContext();
    }

    private void OnClickRemoveNode()
    {
        if (OnRemoveNode != null)
        {
            OnRemoveNode(this);
        }
    }
}