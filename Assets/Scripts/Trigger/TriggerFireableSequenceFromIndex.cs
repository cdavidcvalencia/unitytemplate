﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerFireableSequenceFromIndex : TriggerInterface
{

    public TriggerFireableSequence fireableSequence;
    public int Index;

    public override void Cancel()
    {
        
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        fireableSequence.SetIndex(Index);
        fireableSequence.Fire();
    }

}
