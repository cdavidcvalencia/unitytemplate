﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleTrigger : TriggerInterface
{

    public TriggerInterface TriggerA;
    public TriggerInterface TriggerB;
    public bool FiresB=false;

    public override void Cancel()
    {

    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        if (FiresB)
        {
            TriggerB.Fire(this);
            FiresB = false;
        }
        else
        {
            TriggerA.Fire(this);
            FiresB = true;
        }
    }

#if UNITY_EDITOR
    public override TriggerCallback[] GetCallbacks()
    {
        TriggerCallback[] callbacks = new TriggerCallback[2];
        callbacks[0] = new TriggerCallback("A", TriggerA,0);
        callbacks[1] = new TriggerCallback("B", TriggerB,1);
        return callbacks;
    }

    public override void SetCallback(int index, TriggerInterface trigger)
    {
        if (index == 0)
        {
            TriggerA = trigger;
        }
        else
        {
            TriggerB = trigger;
        }
    }
#endif

}
