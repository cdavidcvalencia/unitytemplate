﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableNextTrigger : TriggerInterface
{

    public GameObject ObjectToDisable;
    public GameObject ObjectToEnable;

    public override void Cancel()
    {
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        ObjectToEnable.SetActive(true);
        ObjectToDisable.SetActive(false);
    }

    public override string GetDescription()
    {
        string desc = base.GetDescription() + "(";
        if (ObjectToDisable != null)
        {
            desc += ObjectToDisable.name;
        }
        else
        {
            desc += "No object assigned";
        }
        if (ObjectToEnable != null)
        {
            desc += ", "+ObjectToEnable.name;
        }
        else
        {
            desc += ", No object assigned";
        }
        desc += ")";
        return desc;
    }

}
