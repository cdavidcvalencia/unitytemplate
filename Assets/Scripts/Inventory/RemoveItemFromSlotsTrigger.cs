﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoveItemFromSlotsTrigger : TriggerInterface
{

    public string ItemName;
    public GameObject SlotsParent;

    public override void Cancel()
    {
        
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        foreach (Transform child in transform)
        {
            DraggableInventorySlot slot=child.GetComponent<DraggableInventorySlot>();
            if (slot != null&&slot.Occupied&& ItemName.Equals(slot.Name))
            {
                slot.ClearSlot();
                return;
            }
        }
    }

}
