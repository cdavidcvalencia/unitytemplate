﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerIfItemsInInventory : TriggerInterface
{

    public Inventory inventory;
    public InventoryItem[] items;
    public TriggerInterface IfTrue;
    public TriggerInterface IfFalse;

    protected Inventory realInventory;

    public override void Cancel()
    {
        
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        if (realInventory == null)
        {
            realInventory = InventoryManager.Instance.getInventory(inventory);
        }
        if (realInventory.HasItems(items))
        {
            if (IfTrue != null)
            {
                IfTrue.Fire();
            }
        }
        else
        {
            if (IfFalse != null)
            {
                IfFalse.Fire();
            }
        }
    }

#if UNITY_EDITOR
    public override TriggerCallback[] GetCallbacks()
    {
        TriggerCallback[] callbacks = new TriggerCallback[2];
        callbacks[0] = new TriggerCallback("True", IfTrue,0);
        callbacks[1] = new TriggerCallback("False", IfFalse,1);
        return callbacks;
    }

    public override  void SetCallback(int index, TriggerInterface trigger)
    {
        if (index == 0)
        {
            IfTrue = trigger;
        }
        else
        {
            IfFalse = trigger;
        }
    }
#endif

}
