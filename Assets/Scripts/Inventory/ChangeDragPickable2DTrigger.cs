﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeDragPickable2DTrigger : TriggerInterface
{

    public DragPickable2D dragPickable2D;
    public bool CanBePicked = true;
    public bool CanReceive = false;

    public override void Cancel()
    {
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        dragPickable2D.CanBePicked = CanBePicked;
        dragPickable2D.CanReceive = CanReceive;
    }

}
