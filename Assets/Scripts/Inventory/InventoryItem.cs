﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class InventoryItem
{

    public string Name="";
    public int Amount;
    public int Tier;

    public InventoryItem(InventoryItem item=null)
    {
        if (item != null)
        {
            Name = item.Name;
            Amount = item.Amount;
            Tier = item.Tier;
        }
    }

    public InventoryItem(InventoryItemData item)
    {
        Name = item.Name;
        Amount = item.Amount;
        Tier = item.Tier;
    }
}
