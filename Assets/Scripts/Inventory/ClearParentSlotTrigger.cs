﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClearParentSlotTrigger : TriggerInterface
{
    
    DraggableInventorySlot slot;

    private void Start()
    {
        Transform trans = transform;
        do
        {
            slot = trans.GetComponent<DraggableInventorySlot>();
            if (slot != null)
            {
                break;
            }
            else
            {
                trans = trans.parent;
                if (trans == null)
                {
                    break;
                }
            }
        } while (true);
    }

    public override void Cancel()
    {
        
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        if (slot != null)
        {
            slot.ClearSlot();
        }
    }

}
