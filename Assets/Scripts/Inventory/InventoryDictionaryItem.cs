﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class InventoryDictionaryItem
{

    public string Name = "";
    public GameObject UIPrefab;
    public int MaxStackAmount = 1;

}
