﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

[System.Serializable]
public class RemoveRandomFromInventoryItem
{

    [Range(0f,100f)]
    public float chances;
    public int MinTier;
    public int MaxTier;
    public int MinAmount;
    public int MaxAmount;

}
