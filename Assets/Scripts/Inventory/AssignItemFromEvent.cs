﻿using mw.player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssignItemFromEvent : TriggerInterface
{

    public DraggableInventorySlot[] slots;

    public override void Cancel()
    {
        
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }

        DraggableInventorySlot slot;

        for (int i = 0; i < slots.Length; i++)
        {
            if (slots[i]!=null&&!slots[i].Occupied)
            {
                slot = slots[i];
                slot.CheckPlayer();
                Inventory inventory = InventoryManager.Instance.getInventory(DraggableInventorySlot.player.inventories[0]);
                inventory.AddItem(StringParameter, 1);
                slot.SetItemInstance(Instantiate(
                    inventory.GetItemUIPrefab(StringParameter)
                ));
                slot.Occupied = true;
                slot.Name = StringParameter;
                slot.Amount = 1;
                PersistanceManager.Instance.SaveData();
                if (slot.onReceiveItem != null)
                {
                    slot.onReceiveItem.Fire();
                }
                break;
            }
        }
    }

}
