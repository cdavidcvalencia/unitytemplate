﻿public class AddInventoriesTrigger : TriggerInterface
{

#if UNITY_EDITOR
    public bool showAddInventoriesTrigger = true;
#endif

    public static string[] TriggerCategories = { "Trigger", "Inventory" };

    public Inventory inventory;
    public InventoryItem[] items;

    protected Inventory realInventory;

    void Start()
    {
        if (realInventory == null)
        {
            realInventory = InventoryManager.Instance.getInventory(inventory);
        }
    }

    public override void Cancel()
    {
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        foreach(InventoryItem item in items)
        {
            realInventory.AddItem(item.Name, item.Amount);
        }
    }

}
