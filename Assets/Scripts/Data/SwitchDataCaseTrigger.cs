﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchDataCaseTrigger : MonoBehaviour
{
    public string StringValue;
    public float FloatValue;
    public int IntValue;
    public bool BoolValue;
    public TriggerInterface Trigger;

    public bool DataCoincides(DataHolder data)
    {
        switch (data.type)
        {
            case DataHolder.Type.Bool:
                if (BoolValue == data.BoolValue)
                {
                    return true;
                }
                break;
            case DataHolder.Type.Integer:
                if (IntValue == data.IntValue)
                {
                    return true;
                }
                break;
            case DataHolder.Type.Number:
                if ((FloatValue-0.01f) <= data.IntValue&& data.IntValue<= (FloatValue + 0.01f))
                {
                    return true;
                }
                break;
            case DataHolder.Type.Text:
                Debug.Log(StringValue + " with: " + data.Value);
                if (
                    (string.IsNullOrEmpty(StringValue) && string.IsNullOrEmpty(data.Value)) ||
                    (StringValue != null && StringValue.Equals(data.Value))
                )
                {
                    Debug.Log("Coinicdes");
                    return true;
                }
                break;
        }
        return false;
    }
}
