﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CompareConstantTrigger : CompareBase
{

#if UNITY_EDITOR
    public bool showCompareConstantTrigger = true;
#endif

    public GameObject Holder;
    public string LabelData;
    public bool DataFromTarget;
    public bool DataOnLeft=true;
    public Operation operation;
    public DataHolder.Type ConstantType;
    public string Value;
    public float NumberValue;
    public int IntegerValue;
    public bool BoolValue;

    public TriggerInterface IfTrue;
    public TriggerInterface IfFalse;

    protected DataHolder TempHolder;

    void Awake()
    {
        TempHolder = gameObject.AddComponent<DataHolder>();
        TempHolder.Name = "Temporal holder";
    }

    public override void StartComparison()
    {
        DataHolder DynamicData;
        TempHolder.type = ConstantType;
        TempHolder.SetValue(Value);
        TempHolder.SetValue(NumberValue);
        TempHolder.SetValue(IntegerValue);
        TempHolder.SetValue(BoolValue);
        if (DataFromTarget)
        {
            DynamicData = GetData(Holder.GetComponent<TargetHolder>().Target, LabelData);
        }
        else
        {
            DynamicData = GetData(Holder, LabelData);
        }
        if (DynamicData == null)
        {
            return;
        }
        if (DataOnLeft)
        {
            Compare(DynamicData, TempHolder, operation, IfTrue, IfFalse);
        }
        else
        {
            Compare(TempHolder, DynamicData, operation, IfTrue, IfFalse);
        }
    }

#if UNITY_EDITOR
    public override TriggerCallback[] GetCallbacks()
    {
        TriggerCallback[] callbacks = new TriggerCallback[2];
        callbacks[0] = new TriggerCallback("True", IfTrue,0);
        callbacks[1] = new TriggerCallback("False", IfFalse,1);
        return callbacks;
    }

    public override void SetCallback(int index, TriggerInterface trigger)
    {
        if (index == 0)
        {
            IfTrue = trigger;
        }
        else
        {
            IfFalse = trigger;
        }
    }
#endif

}
