﻿using UnityEditor;

[CustomEditor(typeof(CompareConstantTrigger))]
public class CompareConstantTriggerEditor : TriggerInterfaceEditor
{
    protected void SetUpPrefabConflict(CompareConstantTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        Undo.RecordObject(trigger, "CompareConstantTrigger");
    }

    protected void StorePrefabConflict(CompareConstantTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        if (!_hadChanges)
        {
            return;
        }
        EditorUtility.SetDirty(trigger);
        PrefabUtility.RecordPrefabInstancePropertyModifications(this);
        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(trigger.gameObject.scene);
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        CompareConstantTrigger compareConstantTrigger = (CompareConstantTrigger)target;

        SetUpPrefabConflict(compareConstantTrigger);

        compareConstantTrigger.showCompareConstantTrigger = EditorGUILayout.Foldout(
            compareConstantTrigger.showCompareConstantTrigger,
            "Compare"
        );
        if (compareConstantTrigger.showCompareConstantTrigger)
        {

            compareConstantTrigger.Holder = EditorUtils.GameObjectField(this, "Holder", compareConstantTrigger.Holder);
            compareConstantTrigger.LabelData = EditorUtils.TextField(this, "Data Name", compareConstantTrigger.LabelData);
            compareConstantTrigger.DataFromTarget = EditorUtils.Checkbox(this, "Is In Target Holder", compareConstantTrigger.DataFromTarget);
            compareConstantTrigger.DataOnLeft = EditorUtils.Checkbox(this, "Is Left Operand", compareConstantTrigger.DataOnLeft);
            compareConstantTrigger.operation = (CompareBase.Operation)EditorUtils.EnumPopup(this, compareConstantTrigger.operation);
            compareConstantTrigger.ConstantType = (DataHolder.Type)EditorUtils.EnumPopup(this, "Constant Type", compareConstantTrigger.ConstantType);

            switch (compareConstantTrigger.ConstantType)
            {
                case DataHolder.Type.Bool:
                    compareConstantTrigger.BoolValue = EditorUtils.Checkbox(this, "Constant Value", compareConstantTrigger.BoolValue);
                    break;
                case DataHolder.Type.Number:
                    compareConstantTrigger.NumberValue = EditorUtils.FloatField(this, "Constant Value", compareConstantTrigger.NumberValue);
                    break;
                case DataHolder.Type.Integer:
                    compareConstantTrigger.IntegerValue = EditorUtils.IntField(this, "Constant Value", compareConstantTrigger.IntegerValue);
                    break;
                case DataHolder.Type.Text:
                    compareConstantTrigger.Value = EditorUtils.TextField(this, "Constant Value", compareConstantTrigger.Value);
                    break;
            }

            compareConstantTrigger.IfTrue = EditorUtils.SelectTrigger(this, "If True", compareConstantTrigger.IfTrue);
            compareConstantTrigger.IfFalse = EditorUtils.SelectTrigger(this, "If False", compareConstantTrigger.IfFalse);

        }

        StorePrefabConflict(compareConstantTrigger);
    }
}
