﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogManager : MonoBehaviour
{

    public GameObject DialogPrefab;
    public DialogType[] Types;

    [HideInInspector]
    public Dictionary<string, DialogType> TypesDictionary=new Dictionary<string, DialogType>();

    void Awake()
    {
        for (int i = 0; i < Types.Length; i++) 
        {
            TypesDictionary.Add(Types[i].Name, Types[i]);
        }
    }

    public DialogType GetType(string type)
    {
        return TypesDictionary[type];
    }

}
