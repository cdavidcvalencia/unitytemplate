﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetSlideValueFromDataTrigger : TriggerInterface
{

    public GameObject Holder;
    public bool FromTarget;
    public string LabelData;
    public Slider slider;

    public override void Cancel()
    {
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        DataHolder[] dataHolders;
        GameObject tempHolder;
        if (Holder != null)
        {
            tempHolder = Holder;
        }
        else
        {
            tempHolder = this.gameObject;
        }
        if (FromTarget)
        {
            tempHolder = tempHolder.GetComponent<TargetHolder>().Target;
        }
        dataHolders = tempHolder.GetComponents<DataHolder>();
        foreach (DataHolder data in dataHolders)
        {
            if (LabelData.Equals(data.Name))
            {
                switch (data.type)
                {
                    case DataHolder.Type.Number:
                        slider.value = data.FloatValue;
                        break;
                    case DataHolder.Type.Integer:
                        slider.value = data.IntValue;
                        break;
                }
                break;
            }
        }
    }

}
