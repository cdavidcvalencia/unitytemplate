﻿using UnityEditor;

[CustomEditor(typeof(SetTextFromDataTrigger), true)]
public class SetTextFromDataTriggerEditor : TriggerInterfaceEditor
{
    protected void SetUpPrefabConflict(SetTextFromDataTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        Undo.RecordObject(trigger, "SetTextFromDataTrigger");
    }

    protected void StorePrefabConflict(SetTextFromDataTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        if (!_hadChanges)
        {
            return;
        }
        EditorUtility.SetDirty(trigger);
        PrefabUtility.RecordPrefabInstancePropertyModifications(this);
        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(trigger.gameObject.scene);
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        _hadChanges = false;
        SetTextFromDataTrigger setTextFromDataTrigger = (SetTextFromDataTrigger)target;
        SetUpPrefabConflict(setTextFromDataTrigger);
        setTextFromDataTrigger.showSetTextFromDataTrigger = EditorGUILayout.Foldout(
            setTextFromDataTrigger.showSetTextFromDataTrigger,
            "Set Text"
        );
        if (setTextFromDataTrigger.showSetTextFromDataTrigger)
        {
            setTextFromDataTrigger.Prefix= EditorUtils.TextField(this, "Prefix", setTextFromDataTrigger.Prefix);

            setTextFromDataTrigger.Holder = EditorUtils.GameObjectField(this, "Holder", setTextFromDataTrigger.Holder);
            setTextFromDataTrigger.LabelData = EditorUtils.TextField(this, "Data Name", setTextFromDataTrigger.LabelData);
            setTextFromDataTrigger.FromTarget = EditorUtils.Checkbox(this, "Is In Target Holder", setTextFromDataTrigger.FromTarget);

            setTextFromDataTrigger.Suffix = EditorUtils.TextField(this, "Suffix", setTextFromDataTrigger.Suffix);

            setTextFromDataTrigger.text = EditorUtils.TextField(this, "UI Text Field", setTextFromDataTrigger.text);
        }
        StorePrefabConflict(setTextFromDataTrigger);
    }
}
