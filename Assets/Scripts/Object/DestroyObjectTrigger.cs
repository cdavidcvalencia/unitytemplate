﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyObjectTrigger : TriggerInterface
{

    public GameObject TheObject;
    public bool OnlyTarget;

    public override void Cancel()
    {
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        if (TheObject != null)
        {
            if (OnlyTarget)
            {
                Destroy(TheObject.GetComponent<TargetHolder>().Target);
            }
            else
            {
                Destroy(TheObject);
            }
        }
    }

    public override string GetDescription()
    {
        string desc = base.GetDescription() + "(";
        if (TheObject != null)
        {
            desc += TheObject.name;
        }
        else
        {
            desc += "No object assigned";
        }
        desc += ")";
        return desc;
    }

}
