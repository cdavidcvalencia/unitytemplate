﻿using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CustomEditor(typeof(IfObjectEnabledTrigger))]
public class IfObjectEnabledTriggerEditor : TriggerInterfaceEditor
{
    protected void SetUpPrefabConflict(IfObjectEnabledTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        Undo.RecordObject(trigger, "IfObjectEnabledTrigger");
    }

    protected void StorePrefabConflict(IfObjectEnabledTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        if (!_hadChanges)
        {
            return;
        }
        EditorUtility.SetDirty(trigger);
        PrefabUtility.RecordPrefabInstancePropertyModifications(this);
        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(trigger.gameObject.scene);
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        IfObjectEnabledTrigger ifObjectEnabledTrigger = (IfObjectEnabledTrigger)target;
        SetUpPrefabConflict(ifObjectEnabledTrigger);
        GameObject prevObject = ifObjectEnabledTrigger.TheObject;
        ifObjectEnabledTrigger.TheObject = (GameObject)EditorGUILayout.ObjectField(
            "Object",
            ifObjectEnabledTrigger.TheObject,
            typeof(GameObject),
            true
        );
        if (prevObject != ifObjectEnabledTrigger.TheObject)
        {
            _hadChanges = true;
        }
        ifObjectEnabledTrigger.IfActive = EditorUtils.SelectTrigger(this,"If Active", ifObjectEnabledTrigger.IfActive);
        ifObjectEnabledTrigger.IfInactive = EditorUtils.SelectTrigger(this,"If Inactive", ifObjectEnabledTrigger.IfInactive);
        StorePrefabConflict(ifObjectEnabledTrigger);
    }
}
