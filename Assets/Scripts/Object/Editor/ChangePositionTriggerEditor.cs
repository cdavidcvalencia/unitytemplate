﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(ChangePositionTrigger))]
public class ChangePositionTriggerEditor : TriggerInterfaceEditor
{

    ChangePositionTrigger changePositionTrigger;
    bool animationTested = false;

    protected void SetUpPrefabConflict(ChangePositionTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        Undo.RecordObject(trigger, "ChangePositionTrigger");
    }

    protected void StorePrefabConflict(ChangePositionTrigger trigger)
    {
        if (EditorApplication.isPlaying)
            return;
        if (!_hadChanges)
        {
            return;
        }
        EditorUtility.SetDirty(trigger);
        PrefabUtility.RecordPrefabInstancePropertyModifications(this);
        UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(trigger.gameObject.scene);
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        changePositionTrigger = (ChangePositionTrigger)target;
        
        SetUpPrefabConflict(changePositionTrigger);
        changePositionTrigger.showChangePositionTrigger = EditorGUILayout.Foldout(
            changePositionTrigger.showChangePositionTrigger,
            "Change Position"
        );
        if (changePositionTrigger.showChangePositionTrigger)
        {
            changePositionTrigger.Object = EditorUtils.GameObjectField(
                this,
                "Object",
                changePositionTrigger.Object
            );
            changePositionTrigger.DataFromTarget = EditorUtils.Checkbox(
                this,
                "Animates Target Holder Instead",
                changePositionTrigger.DataFromTarget
            );
            changePositionTrigger.NewPosition = EditorUtils.TransformField(
                this,
                "Destination",
                changePositionTrigger.NewPosition
            );
            changePositionTrigger.AlsoReparent = EditorUtils.Checkbox(
                this,
                "Also Reparent To Destination",
                changePositionTrigger.AlsoReparent
            );
            changePositionTrigger.Local = EditorUtils.Checkbox(
                this,
                "Local positioning",
                changePositionTrigger.Local
            );
            changePositionTrigger.Animate = EditorUtils.Checkbox(
                this,
                "Animate",
                changePositionTrigger.Animate
            );
            if (changePositionTrigger.Animate)
            {
                changePositionTrigger.showChangePositionTriggerAnimation = EditorGUILayout.Foldout(
                    changePositionTrigger.showChangePositionTriggerAnimation,
                    "Animation"
                );
                if (changePositionTrigger.showChangePositionTriggerAnimation)
                {
                    float[] Time= EditorUtils.MinMaxField(
                        this,
                        "Duration",
                        changePositionTrigger.MinTime,
                        changePositionTrigger.MaxTime
                    );
                    changePositionTrigger.MinTime = Time[0];
                    changePositionTrigger.MaxTime = Time[1];
                    changePositionTrigger.AnimCurveX=EditorUtils.AnimationCurveField(
                        this,
                        "X Curve",
                        changePositionTrigger.AnimCurveX
                    );
                    changePositionTrigger.AnimCurveY = EditorUtils.AnimationCurveField(
                        this,
                        "Y Curve",
                        changePositionTrigger.AnimCurveY
                    );
                    changePositionTrigger.TriggerOnArrive = EditorUtils.SelectTrigger(
                        this,
                        "On Arrive",
                        changePositionTrigger.TriggerOnArrive
                    );
                    if (!animationTested&&GUILayout.Button("Test Animation"))
                    {
                        animationTested = true;
                        changePositionTrigger.TestFire();
                    }
                    if (animationTested&&GUILayout.Button("Reset Position"))
                    {
                        animationTested = false;
                        changePositionTrigger.ResetPosition();
                    }
                }
            }
        }
        StorePrefabConflict(changePositionTrigger);
    }

    void OnDestroy()
    {
        if (animationTested)
        {
            animationTested = false;
            changePositionTrigger.ResetPosition();
        }
    }

}
