﻿using UnityEngine;

public class CameraSizeTrigger : TriggerInterface
{

    public Camera Camera;
    public bool Animate=true;
    public float Speed;
    public float TargetZoom;
    public AnimationCurve AnimCurve;
    public TriggerInterface TriggerOnComplete;

    private bool IsAnimating=false;
    private float StartingZoom;

    public override void Cancel()
    {
        IsAnimating = false;
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        if (!Animate)
        {
            Camera.orthographicSize = TargetZoom;
        }
        else
        {
            StartingZoom = Camera.orthographicSize;
            IsAnimating = true;
        }
    }

    protected virtual void Update()
    {
        if (!IsAnimating)
        {
            return;
        }
        if (
            (Camera.orthographicSize - 0.1f) <= TargetZoom &&
            (Camera.orthographicSize + 0.1f) >= TargetZoom
        )
        {
            Camera.orthographicSize = TargetZoom;
            IsAnimating = false;
            if (TriggerOnComplete != null)
            {
                TriggerOnComplete.Fire(this);
            }
        }
        else
        {
            float timeLapse;
            if (TargetZoom > Camera.orthographicSize)
            {
                if (TargetZoom <= 0.01 && TargetZoom >= -0.01)
                {
                    timeLapse = Camera.orthographicSize / 0.01f;
                }
                else
                {
                    timeLapse = Camera.orthographicSize / TargetZoom;
                }
                Camera.orthographicSize += Speed * AnimCurve.Evaluate(timeLapse) * Time.deltaTime;
            }
            else
            {
                if (Camera.orthographicSize <= 0.01 && Camera.orthographicSize >= -0.01)
                {
                    timeLapse = TargetZoom / 0.01f;
                }
                else
                {
                    timeLapse = TargetZoom / Camera.orthographicSize;
                }
                Camera.orthographicSize -= Speed * AnimCurve.Evaluate(timeLapse) * Time.deltaTime;
            }
        }
    }

#if UNITY_EDITOR
    public override TriggerCallback[] GetCallbacks()
    {
        TriggerCallback[] callbacks = new TriggerCallback[1];
        callbacks[0] = new TriggerCallback("On Complete", TriggerOnComplete,0);
        return callbacks;
    }

    public override void SetCallback(int index, TriggerInterface trigger)
    {
        TriggerOnComplete = trigger;
    }
#endif

}
