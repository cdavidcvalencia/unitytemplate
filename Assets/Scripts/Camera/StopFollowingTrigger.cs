﻿using Cinemachine;

public class StopFollowingTrigger : TriggerInterface
{

    public CinemachineVirtualCamera cinemachineVirtualCamera;

    public override void Cancel()
    {
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        cinemachineVirtualCamera.Follow = null;
    }

}
