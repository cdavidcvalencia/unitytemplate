﻿using Cinemachine;
using UnityEngine;

public class CameraConfinerTrigger : TriggerInterface
{

    public CinemachineConfiner cinemachineConfiner;
    public Collider2D BoundingShape2D;

    public override void Cancel()
    {
    }

    public override void Fire()
    {
        if (!CanTrigger())
        {
            return;
        }
        cinemachineConfiner.m_BoundingShape2D = BoundingShape2D;
    }
}
